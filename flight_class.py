import os
import time
from datetime import datetime
import pandas as pd
from selenium.webdriver.common.by import By
from base_class import DataCollection


class FlightsBoard(DataCollection):
    URL = "https://www.iaa.gov.il/en/airports/ben-gurion/flight-board/"

    def __init__(self):
        super().__init__()
        self.driver.get(self.URL)

    @staticmethod
    def _remove_status(field: str):
        """
        The function receives a string,
        Returns the part of the string that comes formerly to the phrase 'Status...'
        """
        return field[:field.index("Status")]

    @staticmethod
    def _convert_to_ts(timedate: str):
        """
        The function receives a string that represents time
        and returns it legibly after parsing
        """
        v1 = timedate[:6]
        v2 = timedate[6:]
        str_date = f"{v1.strip()} {v2}/{datetime.now().strftime('%Y')}"
        ts = datetime.strptime(str_date, "%H:%M %d/%m/%Y")
        return ts

    def _show_more_results_button(self):
        """
        The function presses on the button 'show more results',
         in order to reveal all the flights
        """
        load_more = self.driver.find_element(By.XPATH, '//*[@id="next"]')
        while load_more.is_displayed():
            time.sleep(2)
            load_more.click()
            time.sleep(2)

    def collect(self):
        time.sleep(5)
        self._show_more_results_button()
        time.sleep(5)
        df = pd.read_html(self.driver.find_element(By.ID, "flight_board-arrivel_table").get_attribute('outerHTML'))[0]
        #  there are additional columns in the site's table ,
        #  which are hidden (for unusual events) and this line removes them from the program's table
        df.drop('Unnamed: 7', inplace=True, axis=1)

        #  removing inaccurate values from the table
        for col in df.columns.values.tolist():
            df[col] = df[col].str.replace(col, "")
            #  the flight element is read from the table with a status expression
            #  this command will omit the expression
            if col == 'Flight':
                df[col] = df[col].apply(self._remove_status)
            # the Scheduled Time is an unreadable time element, the inner function parses it
            if col == 'Scheduled Time':
                df[col] = df[col].apply(self._convert_to_ts)
        # The list contains all the flights, each flight is a dictionary
        list_of_flights = df.to_dict("records")
        self.driver.quit()
        for flight in list_of_flights:
            flight_id = flight['Flight']
            #  writes each flight information into a json file
            with open(f'flights/{flight_id}', 'w') as f:
                f.write(str(flight))

    def search_in_data(self, expression):
        """
        :param expression: string
        :return: list that contains the ID of the files that include the expression
        """
        arr_f = []
        for file in os.listdir("flights"):
            relative_path = "flights" + '/' + file
            with open(relative_path, 'r') as f:
                for line in f:
                    if expression in line:
                        arr_f.append(file)
        return arr_f
