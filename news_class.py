from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import os
from base_class import DataCollection


class NewsBBC(DataCollection):
    URL = "https://www.bbc.com/"

    def __init__(self):
        super().__init__()
        self.driver.get(self.URL)

    @staticmethod
    def _search_in_file(expression, file):
        """The function receives a string expression and a file,
        returns the URL in the first line of the files if the expression is included,
        otherwise None is returned"""
        with open(file, 'r') as f:
            first_line = f.readline()
            for line in f:
                if expression in line:
                    return first_line
            return None

    def collect(self):
        #  articles is a list which contains an object for each article that displays in the home page
        articles = self.driver.find_elements(By.CLASS_NAME, "block-link__overlay-link")
        #  this variable represents the home page
        parent = self.driver.window_handles[0]

        for i in range(0, len(articles)):
            if articles[i].get_attribute('rev') != 'video|overlay':
                #  opens the specific article in a new tab
                articles[i].send_keys(Keys.CONTROL + Keys.ENTER)
                child = self.driver.window_handles[1]
                #  transfers to the page of the article
                self.driver.switch_to.window(child)
                #  article_contents contains the content of the article
                article_contents = self.driver.find_element(By.TAG_NAME, "body").text
                article_path = f'articles/article{i+1}.txt'

                with open(article_path, 'w', encoding="utf-8") as article_file:
                    article_file.write(f"{self.driver.current_url}\n{article_contents}")

                self.driver.close()
                #  transfer back to the home page
                self.driver.switch_to.window(parent)

    def search_in_data(self, expression):
        """
        :param expression: string
        :return: list of URLs of the files that include the expression
        """
        arr_urls = []
        for filename in os.listdir("articles"):
            output = self._search_in_file(expression, f'articles/{filename}')
            if output is not None:
                arr_urls.append(output.strip())
        return arr_urls




