https://www.bbc.com/travel/article/20220815-the-uks-eccentric-book-loving-kingdom
Homepage
Accessibility links
Sign in
Home
News
Sport
Reel
Worklife
Travel
Future
More menu
Search BBC
ADVERTISEMENT
.st0{fill:#FFFFFF;}
Culture
Food
Discovery
Adventure
Destinations
Collections
More
ADVENTURE & EXPERIENCE | MUSEUM | WALES | GREAT BRITAIN | EUROPE
The UK's eccentric book-loving 'kingdom'
Share using Email
Share on Twitter
Share on Facebook
Share on Linkedin
(Image credit: Richard Collett)
By Richard Collett
16th August 2022
In 1977, Richard Booth declared himself "king" of Hay-on-Wye in order to save the Welsh border town from economic ruin. Now, the famed spot is celebrating his legacy.
O
On 1 April 1977, second-hand bookshop owner Richard Booth donned a homemade crown, marched through the streets of Hay-on-Wye and declared himself "King of Hay" outside the gates of the Welsh town's Norman-era castle.
Holding a regal sceptre fashioned from brass plumbing and with the freshly stitched green-and-white flag of the new "Kingdom of Hay" flying behind him, Booth informed bemused residents and press that with himself as "king", independence from the United Kingdom would encourage tourism and revitalise the town's declining economy.
Story continues below
Booth was a showman and a businessman – he saw any opportunity for publicity
Booth issued passports, stamps and currency for his new "nation", his "cabinet ministers" were elected after a few too many drinks in the local pub, and he would later appoint his horse, Goldie, as prime minister. He bestowed citizenship and peerages upon his supporters and fans, and he would rule his self-declared kingdom from the ramparts of Hay Castle until his death in 2019.
"Booth was a showman and a businessman," said Mari Fforde, a local historian who works for the Hay Castle Trust. "He saw any opportunity for publicity. The declaration of independence was part of that keen sense of publicity, his eccentric and iconoclastic streak and his love of attention."
The bookseller's bold bid for independence revived the fortunes of an ailing town. "A lot of small towns were in decline at the time," explained Dr Reg Clark, who worked as Booth's publicist in the 1970s and became the Kingdom of Hay's "Minister for Technology". "The decline was happening in small Welsh towns everywhere. People in rural areas would get an education, but then they would move out and get jobs elsewhere". Booth's marketing ploy brought an influx of curious tourists and bibliophiles to Hay-on-Wye, allowing the second-hand book market to flourish. With a population of just 1,500, the town remains home to some 20-plus bookshops and hosts the United Kingdom's largest literary event, the annual Hay Festival.
Richard Booth declared himself king of the Welsh border town on 1 April 1977 (Credit: Alex Ramsay/Alamy)
Forty five years after his declaration of independence, Booth's crumbling castle – where he lived and ruled – has been transformed into a museum by the Hay Castle Trust. I found myself in Hay-on-Wye on the museum's opening day in May 2022, as Hay Castle opened to the public for the first time in its 900-year history.
Hay Castle's 12th-Century Norman keep, half-fallen away, rises above a grassy mound in the centre of town. In the castle's old moat, I was absorbed in the shelves of an outdoor "Honesty Bookshop" (where every book cost £1, and money is paid into an honesty box) packed with novels, histories, travel narratives, classics, murder mysteries, biographies, recipe books and more.
The Honesty Bookshop's outdoor space doubled up as a street food market in the middle of the Hay Festival, and festivalgoers gorged on falafel and tacos as they read their latest purchases in the shade of Hay Castle. There were more pop-up food stands and market stalls in the surrounding streets, while the main road in front of the castle was lined with second-hand bookshops, antique stores and a fascinating map shop selling cartographic oddities.
The town remains home to some 20-plus bookshops and hosts the annual Hay Festival (Credit: Richard Collett)
From the "Crow's Nest" viewing platform at the top of the castle's tower, I glimpsed the River Wye meandering through the Welsh countryside to the west, while to the south, the Black Mountains rose to form the dramatic border with England. An information board informed me that these are "The Welsh Marches", the lawless borderland between England and Wales that was beset by conflict and ruled by rogue "Marcher Lords" through the Anglo-Saxon, Norman, medieval and Tudor eras.
Booth was like a rogue Marcher Lord in modern guise, and there's no doubt that Hay-on-Wye's location in this murky borderland fuelled his desire for autonomy. "I like to think that Hay-on-Wye has its own identity," Fforde told me later. "It has spent a long time being neither Welsh nor English and still identifies as border town instead."
Born in 1938, the man who would be king moved to a rural estate near Hay-on-Wye when he was young. In his autobiography, My Kingdom of Books (written by Booth and his stepdaughter, Lucia Stuart), Booth explained how he was never destined to succeed in the finance career his parents set him up for in London. Instead, his passion for books led him into the second-hand book trade, and he opened Hay-on-Wye's first bookstore in 1962 after purchasing the Old Fire Station.
I don't think there was any interest in books before Richard. He saw a business opportunity and took it
In 1964, Booth impulsively bought Hay Castle when it came on the market, renovating the historical structure into a bookshop, residence and venue for his many parties. He then took over the town's old agricultural hall – which remains "Richard Booth's Bookshop" to this day – and by 1978, the store was listed in the Guinness Book of Records as the world's largest second hand bookshop with more than one million books stocked at any one time.
"I don't think there was any interest in books before Richard," Fforde said. "He saw a business opportunity and took it. He timed his move into books really well, as many libraries were getting rid of their books at the time. This means that he had stock very cheaply and could stack 'em high."
Hay Castle recently opened to the public for the first time in its 900-year history (Credit: Adrian Seal/Alamy)
By 1976, Hay-on-Wye was marketed by the Welsh tourism board as the world's first "Town of Books", as bookshops sprang up on every street corner. "Booth owned branches and shops all over town," said Clark. "Lots of people came to Hay to work for Richard, and they changed the diaspora and nature of the town. A lot of the local people he trained went onto set up shop on their own."
But the economic reality was tough, as Clark explained. "His major issue was a lack of funds. With second-hand bookshops, things were always lurching from one disaster to the next. That's just the nature of that sort of business." Increasingly disillusioned, Booth believed that government policies were abandoning rural market town economies in favour of new and larger supermarkets and out-of-town shopping centres.
I would be 'Richard Coeur de Livre', the monarch with more pages than most
As Booth's personal financial situation became ever more precarious, he decided it was time for drastic action, so in 1977, Booth informed a visiting reporter he met in the pub that, "Hay is going to be independent of Britain!".
"I would be 'Richard Coeur de Livre'," Booth wrote, "the monarch with more pages than most."
1 April 1977 was an unusual day in Hay-on-Wye. Cannon fire from the micronation's "gunboat" (in reality, a small oar-powered dingy on the River Wye) marked the declaration of independence; a flag was unveiled in front of Hay Castle; and Booth was crowned "King of Hay" as the new national anthem was played.
The declaration may have been made on April Fool's Day, but as Fforde told me, the political reasons behind the stunt were serious. "Personally, I do not think that Booth wanted to be king in any literal sense," she explained. "I would say that he wanted to be king in the sense that he wanted to be listened to and taken seriously, as he had strong opinions about how the rural economy could survive in the climate of the late 20th Century."
Richard Booth's crown jewels are on display at Hay Castle, along with the independence flag and other Kingdom of Hay memorabilia (Credit: Richard Collett)
Visiting Booth's former home, now a museum, I started off exploring the newly opened galleries devoted to Hay Castle's centuries-long history. I was instantly drawn to the top-floor room that contains the "Richard Booth Collection".
Booth's crown jewels are protected by a glass cabinet; the original independence flag hangs on the wall; and there are Kingdom of Hay stamps, currency and passports on display. Although the United Kingdom never recognised Booth's bid for independence, he did receive ambassadors and support from other micronations, including the Principality of Hutt River, a self-declared micronation in Western Australia, and the Free Independent Republic of Frestonia, a micronation formed (also in 1977) to protest the demolition of local houses in London.
Reporters and press flocked to the Kingdom of Hay, and the publicity reinvigorated Hay-on-Wye's economy as tourist crowds gathered to visit the quirky "Town of Books" ruled by a king. But not all in Hay-on-Wye were happy with Booth. "When he declared independence, it didn't necessarily go down well with all the local folks," said Clark. "Booth used to be the darling of the Wales tourist board, but he somehow fell out with them. Hay Town Council put up a notice saying they wouldn't have anything to with the Kingdom of Hay, and that Hay-on-Wye was an integral part of the United Kingdom."
In 1988, Booth even picked a fight with the just-founded Hay Festival, believing that the festival's focus on newly released books was a disservice to the town's second-hand bookshops. Even without the initial blessing of the King of Hay, though, the Hay Festival was a success. It's now the largest literary event of its kind in the UK, and its return in 2022 after a Covid-19 hiatus saw some 500 events staged across two weeks, with 600 speakers and 200,000 ticket sales.
By 1978, Richard Booth's Bookshop was the largest second-hand bookshop in the world (Credit: Richard Collett)
Booth passed away on 20 August 2019, but the "Kingdom of Hay" continues. "There is a lineage for the King," said Fforde. "In 2018, Richard Booth declared his succession, and Ollie Cooke is his successor. The town still has a flag and there have been several Independence weekends – although Covid means we have not had one since 2019."
Booth's real legacy isn't a flag or even a kingdom, but a thriving, book-loving town. "As his stepdaughter Lucia said to me once, he was one of the last great British eccentrics," said Clark. "As his friend, I knew that Richard loved Hay and he wanted the best for it. Not because it was his kingdom, but because he loved the community. He did a lot for Hay, and he brought it out of obscurity."
--
Join more than three million BBC Travel fans by liking us on Facebook, or follow us on Twitter and Instagram.
If you liked this story, sign up for the weekly bbc.com features newsletter called "The Essential List". A handpicked selection of stories from BBC Future, Culture, Worklife and Travel, delivered to your inbox every Friday.
Share using Email
Share on Twitter
Share on Facebook
Share on Linkedin
SHARE
ADVERTISEMENT
RECOMMENDED ARTICLES
The micronation you can't visit
The UK village that you pay to enter
A tiny ‘nation’ of British eccentricity
AROUND THE BBC
FUTURE
The mystery toys puzzling scientists
CULTURE
A prequel that deserves the hype
WORKLIFE
The thinking trap that dupes you
Explore the BBC
Home
News
Sport
Reel
Worklife
Travel
Future
Culture
TV
Weather
Sounds
Terms of Use
About the BBC
Privacy Policy
Cookies
Accessibility Help
Parental Guidance
Contact the BBC
BBC emails for you
Advertise with us
AdChoices / Do Not Sell My Info
Copyright © 2022 BBC. The BBC is not responsible for the content of external sites. Read about our approach to external linking.