https://www.bbc.com/sport/boxing/62612531
Homepage
Accessibility links
Sign in
Home
News
Sport
Reel
Worklife
Travel
Future
More menu
Search BBC
BBC
SPORT
Home
Football
Cricket
Formula 1
Rugby U
Tennis
Golf
Athletics
Cycling
Winter Sports
Commonwealth Games
All Sport
Boxing
Results & Schedule
Calendar
Oleksandr Usyk v Anthony Joshua 2: Briton loses world title rematch by split decision
By Kal Sajad
BBC Sport in Jeddah, Saudi Arabia
Last updated on
14 hours ago
14 hours ago
.
From the section
Boxing
comments1740
Oleksandr Usyk remains undefeated while Anthony Joshua suffers the third defeat of his career
Anthony Joshua's bid to recapture the unified heavyweight titles ended in disappointment as Oleksandr Usyk produced a terrific performance to win by split decision in Jeddah, Saudi Arabia.
In a clash billed as the Rage on the Red Sea, a spirited Joshua, 32, showed some aggressiveness and intent - a vast improvement from their first fight - but could not match the brilliance and ring savviness of the Ukrainian who improved his record to 20 wins from 20 contests.
Two judges scored the fight 115-113 and 116-112 to Usyk and the third gave it 115-113 to Joshua.
While there were some close rounds, Usyk - in just his fourth bout since moving up from cruiserweight to heavyweight - was the deserved winner.
At the end of the fight, a furious Joshua - quite bizarrely - picked up two of Usyk's belts, dropped them out of the ring and strode towards the dressing room, before turning round and getting back into the ring to give an impassioned speech.
"Usyk is one hell of a fighter. That's just emotion," he said. "If you knew my story, you'd understand the passion. I'm not an amateur boxer. I was going to jail and I got bail and I started training.
Joshua breaks down in his post-fight press conference after loss to Usyk
"It shows the passion we put into this. For this guy to beat me tonight, it shows the levels of hard work he must have put in, so please give him a round of applause as heavyweight champion of world.
"They said that I'm not a 12-round fighter. I ain't 14 stone, I'm 18 stone, I'm heavy. It's hard work. This guy here is phenomenal."
Joshua also went up to Usyk and said to him: "I was studying Ukraine and all the champions from your amazing country. I've never been there. What's happening there, I don't know but it's not nice.
"You're not strong, how did you beat me? Because of skills. I had character and determination."
He has now suffered back-to-back defeats against Usyk, with the champion retaining the WBA (Super), WBO and IBF titles he won in London last September.
As it happened: Usyk v Joshua 2
Smith delivers brutal KO against Bauderlique
Usyk calls out Fury
Usyk says he only wants to fight Tyson Fury
Joshua had some success in the fight - and enjoyed his best round in the ninth, charging Usyk down and unloading a flurry of punches, reminiscent of the AJ of old.
But such is the brilliance of Usyk, he came back fighting in the 10th round and was landing clean blow after blow. Usyk landed a five-punch combination, and Joshua became a sitting duck.
Joshua fatigued and Usyk - who just a few months ago was defending his nation against the Russian invasion - took full advantage in the championship rounds.
"I devote this victory to my family, my country, my team, to all the military defending this country - thank you so much," he said, before calling out WBC champion Tyson Fury.
"I'm sure that Tyson Fury is not retired yet. I'm convinced he wants to fight me. I want to fight him. If I'm not fighting Fury, I'm not fighting at all.
"Only God knows whether I will fight him or not but all these gentlemen here around me, my team, they are going to help me."
Fury recently announced his retirement but has until 26 August to decide on whether he will relinquish the belt.
The Gypsy King criticised both fighters on social media, and hinted he may not yet be done with boxing. The carrot of a clash with Usyk - one which would certainly cement the winner's legacy as an all-time great - is dangling.
Tyson Fury reacts to Anthony Joshua's defeat by Oleksandr Usyk
Usyk shines in Jeddah
The boxers made their ring walks at approximately 01:00 local time. Even in those early hours, there were temperatures of 30C, as approximately 12,000 fans filled out the air-conditioned King Abdullah Sport City Arena.
A crowd that was somewhat subdued for most of the night came alive as Joshua - the challenger - marched to the ring first. Jeers echoed around the arena as Usyk followed. It was clear who the fan favourite was.
Heading into the fight, there were questions as to how well Joshua and new trainer Robert Garcia had gelled, having only started working together this year.
The opening few rounds were close, but ones which Usyk most likely edged. After three rounds, American Garcia told Joshua he had won every round.
The champion was given time to recover in the fifth round after a Joshua low blow had him wincing. A two-punch combination to Joshua's midriff and head, from range, was the pick of the sixth round.
As the fight progressed, Usyk's movement started to dictate the fight. He landed a counter left in the seventh, before Joshua unloaded two smart shots to the body in the eighth.
Then came the two of the most memorable rounds, out of the 24 across two bouts, between the two fighters. Joshua gave it everything in the ninth, and appeared to have Usyk in a spot of bother. Perhaps it was a ploy from Usyk to tire Joshua out. In the 10th, he broke Joshua's resolve - and went on to take the fight.
Did Joshua answer his critics?
Joshua was upset after the decision and spoke directly to the crowd
Before the fight, Joshua and promoter Eddie Hearn insisted defeat would not spell retirement but described the bout as must-win.
After a third loss in five bouts, Joshua now has to resurrect his career. But he can take positives from his performance. His reputation and legacy have been dealt a blow, but perhaps not as heavy as many predicted.
In June 2019, Joshua suffered a shocking knockout defeat by Andy Ruiz Jr in New York. Six months later, in Riyadh, Saudi Arabia, Joshua recaptured his heavyweight crown with a disciplined points win.
Beating Usyk, however, was a totally different challenge. Joshua was in against a pound-for-pound star - the trickiest of southpaws. And he was facing a patriotic Ukrainian determined to bring some much-needed joy to a nation torn apart through war.
Boxing fans wanted to see some heart and fight from Joshua. In the 10th round, when under attack from Usyk, he barked "come on" at his opponent.
Such is the beast of heavyweight boxing and the level of fame bestowed on Joshua, there will always be critics. This time, though, perhaps not for his performance, but for his post-fight interview.
How many friends do you need? Evolutionary psychologist Robin Dunbar has some answers
Bargain smiles or big mistake? The latest trend of so-called 'Turkey teeth' is examined
commentsView Comments1741














Top Stories
Premier League: Chelsea trail Leeds 3-0 & Brighton extend advantage
Live
LIVE
From the section
Football
Scottish Premiership: Celtic 1-0 Hearts - Kyogo flashes hosts in front
Live
LIVE
From the section
Football
Watch: European Championships - Gymnastics, canoeing & beach volleyball
Live
LIVE
From the section
Sport
Elsewhere on the BBC
Who stole Tamara Ecclestone's diamonds worth £26m?
Follow the international police investigation hunting down the burglars targeting celebrities
Is marrying your cousin really that bad?
Mona Chalabi looks at the genetic consequences and taboos of cousin marriage
A tale of gangland feuds and bloody reprisals
The rise and inevitable fall of Curtis Warren, one of Britain's biggest and wealthiest drug barons
Also in Sport
Joshua's emotional post-fight news conference
'I'd annihilate both' - Fury reacts to Joshua loss
'Absolute class' - how Zinchenko starred for Arsenal
'Finally!' - GB's Hodgkinson cruises to 800m gold
Arsenal mentality shift 'crazy' - how far can they go?
The night superstar Rashid lit up The Hundred
Superstar or superflop? Will Casemiro fit Man Utd?
England outplayed by 'superb' South Africa - highlights
Five young English footballers to watch out for
'Why is menstruation still a taboo subject?'
'Silky smooth' - GB pair take gold and silver in 200m
GB's Muir wins gold in 1500m final
Time for Aubameyang? Do Chelsea need a striker?
GB's Asher-Smith wins 200m silver in close race
Emotional scenes as GB's Okoye wins bronze in discus
'Ronaldo shouldn't be at Man Utd but has nowhere to go'
Explore the BBC
Home
News
Sport
Reel
Worklife
Travel
Future
Culture
TV
Weather
Sounds
Terms of Use
About the BBC
Privacy Policy
Cookies
Accessibility Help
Parental Guidance
Contact the BBC
BBC emails for you
Advertise with us
AdChoices / Do Not Sell My Info
Copyright © 2022 BBC. The BBC is not responsible for the content of external sites. Read about our approach to external linking.