https://www.bbc.com/news/science-environment-62582186
BBC Homepage
Skip to content
Accessibility Help
Sign in
Home
News
Sport
Reel
Worklife
Travel
Future
More menu
Search BBC
BBC News
Home
War in Ukraine
Coronavirus
Climate
Video
World
UK
Business
Tech
Science
Stories
More
Science
UK drought: Why do the trees think it's autumn already?
By Helen Briggs
Environment correspondent
Published
1 day ago
Share
IMAGE SOURCE,
GETTY IMAGES
Image caption,
Early leaf fall in London
From the crunch of leaves underfoot and the fiery foliage adorning the trees, you might be thinking autumn has come early.
But experts say this hint of a change in the seasons isn't genuine. Instead it's the tell-tale sign of a "false autumn".
They warn the heatwave and drought has pushed trees into survival mode, with leaves dropping off or changing colour as a result of stress.
And some may end up dying as a result.
Why we need to get used to wonky vegetables
Wildlife under stress as dry spell shrinks rivers
Sewage hits dozens of UK beaches after heavy rain
IMAGE SOURCE,
GETTY IMAGES
Image caption,
Dying tree in a London park
Auburn leaves and early leaf fall are both signs that trees are stressed and "shutting up shop", says Leigh Hunt, senior horticultural advisor at the Royal Horticultural Society.
"It's giving the appearance that we're already in autumn, but the days are too long for those natural autumn processes to begin," he says.
"Physiologically, the plants are not responding to autumn conditions; that's why we term it loosely as 'false autumn'."
IMAGE SOURCE,
GETTY IMAGES
Image caption,
Old tree skeletons exposed due to low water levels at Colliford Lake near Bodmin, Cornwall
He says in all his 45 years, this is one of the most severe years he's seen in terms of damage to trees in the countryside.
And while established trees can withstand drought through their extensive network of roots, younger specimens, such as those planted on poor soil at the edge of roads could wither and die.
'Second spring'
Trees that have lost only a few leaves with just a little bit of yellowing should recover with enough rainfall, he explains.
There's "a critical point", though, when the tree can't replenish the water lost through pores in the leaves and will "literally desiccate" or dry up.
In conditions like those seen recently, trees may react by producing more seeds - for instance, acorns - in an attempt to reproduce and survive into the future.
IMAGE SOURCE,
GETTY IMAGES
Image caption,
The drought has caused hard, compacted soil, which can affect germination
And if there's a lot of rain we might even see "a second spring" with trees putting on an extra spurt of growth, he says.
Early berries
Other signs of the unpredictable weather can be seen in berries appearing on plants and shrubs.
The Woodland Trust, which records seasonal changes, has received its earliest ever report of ripe blackberries - from 28 June.
It says fruits and nuts are ripening faster than ever, which "can spell disaster for wildlife" that feed on them.
"The record-breaking heat we have just experienced has helped bring on a number of early autumn events," says Fritha West, from the Woodland Trust.
"We have received some of our earliest ever ripe blackberry records from the south of England. Hawthorn and rowan are also ripening early in some parts of the country, where early leaf tinting has also been observed.
"Elder and holly have been recorded as fruiting earlier too. Both extreme temperatures and a lack of water can cause trees to drop their leaves earlier than we'd expect."
Shrinking rivers
It's hard to predict the long-term consequences of drought, but ecology experts think weeks of parched grasslands and rock-hard soil in much of southern England will be having a big impact on wildlife.
IMAGE SOURCE,
GETTY IMAGES
Image caption,
The shell of a crayfish on the dried river bed of the Thames in Ashton Keyne
In and around rivers, the drought could be felt for years to come.
During summer droughts, fast-growing algae can smother wetland plants, killing them by blocking out light.
A reduction in river levels reduces habitat for fish, amphibians and invertebrates, affecting whole ecosystems.
"These plants provide vital habitat for insects and fish, and their loss from the ecosystem causes major changes up the food chain," says Dr Mike Bowes of the UK Centre for Ecology & Hydrology.
"It can take several years before plants are able to recover or recolonise these drought-impacted river reaches, and so the impact of severe droughts can be prolonged."
Follow Helen on Twitter @hbriggs.
Related Topics
Trees
Drought
Nature
Environment
Top Stories
Daughter of Putin ally killed in Moscow blast
Published
1 hour ago
Somalia hotel siege ends leaving more than 20 dead
Published
9 minutes ago
Singapore to end ban on gay sex
Published
5 minutes ago
Features
Demi Lovato is back, and this time she's angry
Greece promotes smaller islands over Mykonos and Corfu
Missing girl found after nine years describes ordeal
The new sound of dance music sweeping the world. Video
The new sound of dance music sweeping the world
'Everything we get is through selling cannabis'
'I broke the world record with my last ever run' Video
'I broke the world record with my last ever run'
Why one of the world's greatest composers was erased from history
Peace at a price in the Taliban’s heartlands
How much grain is being shipped from Ukraine?
Elsewhere on the BBC
Turkey's massive subterranean city
The language with no known origin
Why overthinkers struggle with remote work
Most Read
1
Daughter of Putin ally killed in Moscow blast
2
Singapore to end ban on gay sex
3
In pictures: Drought exposes sunken treasures
4
Missing girl found after nine years recounts ordeal
5
Map may show evidence of sunken Welsh kingdom
6
Somalia hotel siege ends leaving more than 20 dead
7
Dock workers striking for first time in 30 years
8
Drone attack on Russian base in Crimea
9
State of alert in Portugal amid wildfire risk
10
Greece promotes smaller islands over Mykonos and Corfu
BBC News Services
On your mobile
On smart speakers
Get news alerts
Contact BBC News
Home
News
Sport
Reel
Worklife
Travel
Future
Culture
Music
TV
Weather
Sounds
Terms of Use
About the BBC
Privacy Policy
Cookies
Accessibility Help
Parental Guidance
Contact the BBC
Get Personalised Newsletters
Why you can trust the BBC
Advertise with us
AdChoices / Do Not Sell My Info
© 2022 BBC. The BBC is not responsible for the content of external sites. Read about our approach to external linking.