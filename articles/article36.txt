https://www.bbc.com/news/science-environment-62606589
BBC Homepage
Skip to content
Accessibility Help
Sign in
Home
News
Sport
Reel
Worklife
Travel
Future
More menu
Search BBC
BBC News
Home
War in Ukraine
Coronavirus
Climate
Video
World
UK
Business
Tech
Science
Stories
More
Science
Robot boat maps Pacific underwater volcano
By Sophie Ormiston
Science writer
Published
1 day ago
Share
Related Topics
Tonga eruption and tsunami
Media caption,
Watch USV Maxlimer as it moves through waters around Hunga-Tonga Hunga-Ha’apai
A robot boat, controlled from the UK, has returned from an initial survey of the underwater Tongan volcano that erupted explosively back in January.
The Uncrewed Surface Vessel (USV) Maxlimer is part-way through mapping the opening, or caldera, of the underwater Hunga-Tonga Hunga-Ha'apai (HTHH) volcano.
The vessel, developed by the British company Sea-Kit International, is surveying the volcano as part of the second phase of the Tonga Eruption Seabed Mapping Project (TESMaP), led by New Zealand's National Institute of Water and Atmospheric Research (Niwa) and funded by the Nippon Foundation of Japan.
IMAGE SOURCE,
SEA-KIT/NIWA/NIPPON FOUNDATION/SEABED2030
Image caption,
The boat is gradually building up a bathymetric (depth) map of the volcano's opening, or caldera
The data collected so far has confirmed earlier reports of continuing volcanic activity from HTHH. A winch on the boat allows instruments to be deployed at depths, reaching 300m, in order to collect data from the entire water column.
The 12m-long Maxlimer may be in Tonga, but it is being remotely controlled from 16,000km away in the small coastal village of Tollesbury in Essex. Everything is done over satellite link.
In a dark control room in Sea-Kit's HQ, several large screens display live feed images from the 10 cameras on board Maxlimer. Operators, who work in shifts around the clock, watch on as real-time data gets beamed in from the South Pacific.
Media caption,
"We can communicate with nearby vessels via radio as if we were on board"
Immense crater hole created in Tonga volcano
Tonga eruption was 'record atmospheric explosion'
Tonga's volcanic plume reached half-way to space
Ashley Skett, director of operations at Sea-Kit, told BBC Radio 4's Inside Science programme that the operators can even communicate via radio with other vessels in the area, so much so that other boats wouldn't know that nobody was on board Maxlimer.
"The boat has been designed from the ground up to be remotely controlled and remotely operated. So every switch, every function on the boat, every light, we can control from here."
Robotic, remotely controlled boats are likely to be the future of maritime operations. When surveying a dangerous area such as the active HTHH volcano, remote control ensures no crew are put in harm's way.
IMAGE SOURCE,
SEA-KIT/NIWA/NIPPON FOUNDATION/SEABED2030
Image caption,
When not at sea, Maxlimer is moored at Nuku'alofa on Tongatapu, Tonga's main island
There are also environmental benefits to USVs. As there is no onboard crew to support, the vessel can be much smaller, leading to reduced carbon dioxide emissions.
"We use 5% of the fuel that an equivalent manned vessel doing the same job that we're doing now would use," Ashley said.
The eruption of HTHH in January caused extensive damage far beyond Tonga, triggering a massive tsunami that spread across the Pacific Ocean. The atmospheric shockwave caused by the eruption was felt as far away as the UK.
Media caption,
Weather satellites captured the climactic eruption. Video looped three times. (Himawari-8/JMA/NCEO/@simon_sat)
Maxlimer is currently taking a short break while some rough weather passes through the region. Once conditions improve, the boat will head back out to the underwater volcano to fill in remaining gaps in its map of the caldera.
The collected data will help us understand why the eruption had such a huge and violent impact, as well as help predict the nature of future eruptions.
More on this story
Immense crater hole created in Tonga volcano
25 May
Explosive Tonga volcano 'surprisingly intact'
23 May
Tonga eruption was 'record atmospheric explosion'
15 May
Professor snorkels over Tonga volcano
11 April
Tonga internet link restored after eruption
22 February
Tonga's volcanic plume reached half-way to space
21 January
Related Topics
Earth science
Volcanoes
Tollesbury
Tsunamis
Tonga eruption and tsunami
Robotics
Geology
Tonga
Top Stories
Daughter of Putin ally killed in Moscow blast
Published
7 minutes ago
Somalia deadly hotel siege ends, officials say
Published
57 minutes ago
Strike begins at UK's biggest container port
Published
1 hour ago
Features
Demi Lovato is back, and this time she's angry
Greece promotes smaller islands over Mykonos and Corfu
Missing girl found after nine years describes ordeal
The new sound of dance music sweeping the world. Video
The new sound of dance music sweeping the world
'Everything we get is through selling cannabis'
'I broke the world record with my last ever run' Video
'I broke the world record with my last ever run'
Why one of the world's greatest composers was erased from history
Peace at a price in the Taliban’s heartlands
How much grain is being shipped from Ukraine?
Elsewhere on the BBC
Turkey's massive subterranean city
The language with no known origin
Why overthinkers struggle with remote work
Most Read
1
Daughter of Putin ally killed in Moscow blast
2
Missing girl found after nine years recounts ordeal
3
Map may show evidence of sunken Welsh kingdom
4
Strike begins at UK's biggest container port
5
Somalia deadly hotel siege ends, officials say
6
State of alert in Portugal amid wildfire risk
7
Drone attack on Russian base in Crimea
8
Greece promotes smaller islands over Mykonos and Corfu
9
Joshua rants after split-decision defeat by Usyk
10
Bus and truck ram rescuers in separate crashes
BBC News Services
On your mobile
On smart speakers
Get news alerts
Contact BBC News
Home
News
Sport
Reel
Worklife
Travel
Future
Culture
Music
TV
Weather
Sounds
Terms of Use
About the BBC
Privacy Policy
Cookies
Accessibility Help
Parental Guidance
Contact the BBC
Get Personalised Newsletters
Why you can trust the BBC
Advertise with us
AdChoices / Do Not Sell My Info
© 2022 BBC. The BBC is not responsible for the content of external sites. Read about our approach to external linking.