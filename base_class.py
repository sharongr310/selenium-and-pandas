from abc import ABC, abstractmethod
from selenium import webdriver


class DataCollection(ABC):
    PATH = r"C:\Program Files (x86)\chromedriver.exe"

    @abstractmethod
    def __init__(self):
        self.driver = webdriver.Chrome(self.PATH)

    @abstractmethod
    def collect(self):
        """ The function enters a site, collects the needed information and documents it into a file.
            Every site has its relative directory.
        """
        pass

    @abstractmethod
    def search_in_data(self, expression):
        """
        The function receives an expression and returns the files that contain the expression.
        expression: string
        :return: news return a URL, flights return an ID
        """
        pass